(function($){

    App.Widgets.WsClient = can.Control.extend(
        {
            pluginName: 'WsClient',
            defaults: {
                serverUri: 'ws://127.0.0.1:11000'
            }
        },
        {
            init: function () {
                this.socket = null;
                this.socketReady = false;

                this.userData = false;
                this.userDataDef = $.Deferred();

                this.createSocket().then(this.proxy(function () {
                    this.getUser().then(this.proxy(function () {
                        this.registerClient();
                    }));
                }));

                this.popup = new App.Widgets.MessagePopup('.js-message-popup');
            },

            createSocket: function () {
                var def = $.Deferred();

                this.socket = new WebSocket(this.options.serverUri);

                this.socket.onopen = this.proxy(function(e) {
                    this.socketReady = true;
                    def.resolve();
                });

                this.socket.onerror = this.proxy(function(e) {
                    console.log(e);
                });

                this.socket.onmessage = this.proxy(this.onMessage);

                return def.promise();
            },

            onMessage: function (e) {

                var data = JSON.parse(e.data);

                if (!data || data.length === 0) {
                    return;
                }

                switch (data.action) {
                    case 'user-data':
                        this.userData = data.data;
                        this.userDataDef.resolve();

                        this.element.find('.js-user-info').html(can.view('tplUserInfo', this.userData));
                        break;

                    case 'new-message': {
                        this.element.find('.js-body').append(can.view('tplMessage', data));

                        if (!this.popup.opened) {
                            this.popup.show();
                        }
                        break;
                    }
                }
            },

            send: function (action, params) {

                if (!this.socketReady) {
                    console.log('Socket is not ready to send/receive data');
                    return;
                }

                params = params || {};

                this.socket.send(JSON.stringify({
                    route: 'web-client/' + action,
                    params: params
                }));
            },

            getUser: function () {
                this.send('get-new-user');

                return this.userDataDef.promise();
            },

            registerClient: function () {

                this.send('register-client', {
                    id: this.userData.id,
                    tasks: this.userData.tasks
                });

            }
        }
    );


    App.Widgets.MessagePopup = App.Widgets.BootstrapModal.extend({pluginName: 'MessagePopup'}, {});

}(jQuery));