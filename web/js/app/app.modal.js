(function($){

    var currentModal = null;

    App.Widgets.BootstrapModal = can.Control.extend(
        {
            defaults: {
                caller: null
            },
            listensTo: ['show.bs.modal', 'hide.bs.modal']
        },
        {
            init: function () {
                this.caller = null;
                this.opened = false;
                this.on();

                if (this.options.caller) {
                    $('body').on('click', this.options.caller, this.proxy(this.onCallerClick));
                }
            },

            onCallerClick: function (e) {
                e.preventDefault();

                this.caller = $(e.currentTarget);
                this.show();
            },

            onShow: function () {

            },

            onHide: function () {

            },

            show: function () {
                this.element.modal('show');
            },

            hide: function () {
                this.element.modal('hide');
            },

            'show.bs.modal': function (m, e) {
                if (currentModal) {
                    currentModal.modal('hide');
                }
                currentModal = this.element;
                this.opened = true;
                this.onShow();
            },

            'hide.bs.modal': function () {
                currentModal = null;
                this.caller = null;
                this.opened = false;
                this.onHide();
            }
        }
    );

}(jQuery));