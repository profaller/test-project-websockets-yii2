(function($){

    window.App = {
        Widgets: {},
        Instance: {}
    };

    var Application = can.Control.extend({}, {

        bootstrap: function () {
            this.initWidgets();
        },

        /**
         * @param selector
         * @param controllerName
         * @param settings
         * @returns {*}
         */
        installController: function (selector, controllerName, settings) {
            settings = settings || {};
            App.Instance[controllerName] = this.element.find(selector)[controllerName](settings).control(controllerName);
        },

        initWidgets: function () {
            this.element.find('[data-app-controller]').each(this.proxy(function (i, el) {
                var $el = $(el),
                    ctrl = $el.data('app-controller'),
                    opts = $el.data('controller-options');

                this.installController($el, ctrl, opts);
            }));
        }

    });


    $(function(){
        window.application = new Application('html');
        window.application.bootstrap();
    });

}(jQuery));


window.dbg = function () {
    console.log.apply(console, arguments);
};
