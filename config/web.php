<?php

use app\client\Module;

$config = [
    'id' => 'wsClient',
    'name' => 'WS Client',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'client/web/index',

    'modules' => [
        'client' => [
            'class' => Module::class,
            'serverUri' => '127.0.0.1:11000',
        ]
    ],

    'components' => [
        'request' => [
            'cookieValidationKey' => 'noooo'
        ]
    ]
];

return $config;
