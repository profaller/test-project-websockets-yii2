<?php

use app\client\components\WsClient;
use app\client\controllers\ConsoleController;
use app\server\commands\ConsoleCommands;
use app\server\components\WsService;


$config = [
    'id' => 'webSocketApp',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],

    'modules' => [

        'server' => [
            'class' => \app\server\Module::class,
            'defaultRoute' => 'server/start',
            'controllerMap' => [
                'server' => [
                    'class' => ConsoleCommands::class
                ],
            ],
            'components' => [
                'wsServer' => [
                    'class' => WsService::class,
                    'serverUri' => '127.0.0.1:11000',
                ],
            ]
        ],

        'client' => [
            'class' => \app\client\Module::class,
            'serverUri' => '127.0.0.1:11000',

            'controllerMap' => [
                'command' => [
                    'class' => ConsoleController::class
                ],
            ],
            'components' => [
                'wsClient' => [
                    'class' => WsClient::class,
                    'serverControllerName' => 'console-client',
                ],
            ],
        ],

    ],

    /*'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],*/
];

return $config;
