Установка
------------

~~~
composer install
~~~

WS Сервер
-------------

~~~
php yii server
~~~


WS Клиент
-------------

### Web

Доступен на `http://youdomain`


### Console

Все ID пользователей
~~~
php yii client/command/get-all-users
~~~

Все таски пользователя
~~~
php yii client/command/get-all-user-tasks --userId={userID}
~~~


Отправить сообщение
~~~
php yii client/command/send-message --userId={userID|all} [--taskId={taskID}] --message="{message}"
~~~


