<?php

/* @var $this yii\web\View */

use app\client\Module;
use yii\helpers\Json;

?>

<div class="client-index"
     data-app-controller="WsClient"
     data-controller-options="<?= Json::encode(['serverUri' => 'ws://' . Module::getInstance()->serverUri ])?>">


    <div class="container js-user-info">

    </div>

    <script type="text/ejs" id="tplUserInfo">
        <div class="user">User ID: #<%= id %></div>
        <div class="tasks">Tasks: <%= tasks.join(', ') %></div>
    </script>

    <script type="text/ejs" id="tplMessage">
        <div class="message" style="margin-bottom: 10px; padding-top: 10px;">
            <div class="row">
                <div class="col-md-2">To task:</div>
                <div class="col-md-10">#<%= taskId ? taskId : 'all' %></div>
            </div>
            <div class="row">
                <div class="col-md-2">Message:</div>
                <div class="col-md-10"><%= message %></div>
            </div>
        </div>
    </script>


    <div class="modal fade js-message-popup" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title pull-left">Messages</h4>
                    <button type="button" class="modal-default-close close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body js-body">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-xs" data-dismiss="modal" aria-hidden="true">Ok</button>
                </div>

            </div>
        </div>
    </div>

</div>
