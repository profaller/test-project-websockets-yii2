<?php

namespace app\client\components;


use app\client\Module;
use yii\base\Component;
use yii\helpers\Json;

class WsClient extends Component
{
    public $serverControllerName;


    public function send($action, array $params = [])
    {
        \Ratchet\Client\connect('ws://' . Module::getInstance()->serverUri)->then(function($conn) use ($action, $params) {

            $conn->on('message', function ($response) use ($conn) {
                echo "$response\r\n";
                $conn->close();
            });

            $conn->send(Json::encode([
                'route' => $this->serverControllerName . '/' . $action,
                'params' => $params,
            ]));

        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });
    }
}