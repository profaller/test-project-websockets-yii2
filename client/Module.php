<?php

namespace app\client;


class Module extends \yii\base\Module
{
    public $controllerNamespace = '\app\client\controllers';

    public $serverUri = '127.0.0.1:11000';


    public function init()
    {
        parent::init();
    }
}