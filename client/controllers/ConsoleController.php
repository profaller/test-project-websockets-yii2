<?php

namespace app\client\controllers;


use app\client\Module;
use yii\console\Controller;

class ConsoleController extends Controller
{
    public $message;

    public $userId;

    public $taskId;


    public function options($actionID)
    {
        return ['message', 'userId', 'taskId'];
    }

    public function optionAliases()
    {
        return [
            'm' => 'message',
            'u' => 'userId',
            't' => 'taskId',
        ];
    }



    public function send($action, $params = [])
    {
        Module::getInstance()->wsClient->send($action, $params);
    }

    public function actionGetAllUsers()
    {
        $this->send('users');
    }

    public function actionGetAllUserTasks()
    {
        $this->send('tasks', [
            'userId' => $this->userId
        ]);
    }

    public function actionSendMessage()
    {
        $data['message'] = $this->message;

        if ($this->taskId) {
            $data['taskId'] = (int) $this->taskId;
        }
        if ($this->userId && !$this->userId != 'all') {
            $data['userId'] = (int) $this->userId;
        }

        $this->send('send-message', $data);
    }
}
