<?php

namespace app\client\controllers;


use yii\web\Controller;

class WebController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
