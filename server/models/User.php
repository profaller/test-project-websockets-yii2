<?php

namespace app\server\models;


use Ratchet\ConnectionInterface;
use yii\base\Model;

class User extends Model
{
    /**
     * @var
     */
    public $id;

    /**
     * @var array
     */
    public $tasks;

    /**
     * @var ConnectionInterface
     */
    public $conn;


    public function hasTask($taskId)
    {
        return in_array($taskId, $this->tasks);
    }
}