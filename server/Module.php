<?php

namespace app\server;


class Module extends \yii\base\Module
{
    public $controllerNamespace = '\app\server\controllers';
}