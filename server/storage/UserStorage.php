<?php

namespace app\server\storage;

use app\server\models\User;


/**
 * Class UserStorage
 * @package app\server
 */
class UserStorage extends \SplObjectStorage
{
    private $lastUserId = 0;

    private $lastTaskId = 0;


    /**
     * @return int
     */
    protected function getNewUserId()
    {
        return ++$this->lastUserId;
    }

    /**
     * @return int
     */
    protected function getNewTaskId()
    {
        return ++$this->lastTaskId;
    }

    /**
     * @return array
     */
    public function generateNewUserData()
    {
        $tasks = [];
        for ($i = 0; $i < rand(1, 5); $i++) {
            $tasks[] = $this->getNewTaskId();
        }

        return [
            'id' => $this->getNewUserId(),
            'tasks' => $tasks,
        ];
    }



    /**
     * @return User[]
     */
    protected function getRegisteredUsers()
    {
        $result = [];
        foreach ($this as $client) {
            $user = $this[$client];

            if ($user instanceof User) {
                $result[$user->id] = $user;
            }
        }

        return $result;
    }

    /**
     * @param $id
     * @return User|boolean
     */
    public function getUser($id)
    {
        $users = $this->getRegisteredUsers();
        if (isset($users[$id])) {
            return $users[$id];
        }

        return false;
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->getRegisteredUsers();
    }
}