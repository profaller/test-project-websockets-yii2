<?php

namespace app\server\components;


use app\server\ServerApplication;
use yii\base\Component;
use React\EventLoop\Factory;
use React\Socket\Server;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

class WsService extends Component
{
    public $serverUri;


    public function startServer()
    {
        $eventLoop = Factory::create();

        $socket = new Server($this->serverUri, $eventLoop);

        $server = new IoServer(
            new HttpServer(
                new WsServer(
                    new ServerApplication()
                )
            ),
            $socket,
            $eventLoop
        );

        $server->run();
    }
}