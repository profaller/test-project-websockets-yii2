<?php

namespace app\server\commands;


use app\server\Module;
use yii\console\Controller;

class ConsoleCommands extends Controller
{
    public function actionStart()
    {
        Module::getInstance()->wsServer->startServer();
    }
}
