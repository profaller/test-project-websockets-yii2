<?php

namespace app\server;


use app\server\components\WsController;
use app\server\storage\UserStorage;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use yii\helpers\Json;

class ServerApplication implements MessageComponentInterface
{
    /**
     * @var UserStorage
     */
    protected $userStorage;


    /**
     * SocketServer constructor.
     */
    public function __construct()
    {
        $this->userStorage = new UserStorage();
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->userStorage->attach($conn);
    }

    /**
     * @param ConnectionInterface $from
     * @param string $message
     */
    public function onMessage(ConnectionInterface $from, $message)
    {
        $request = Json::decode($message);
        if (!is_array($request) || !isset($request['route'])) {
            return;
        }

        $route = $request['route'];
        $params = isset($request['params']) && is_array($request['params']) ? $request['params'] : [];

        /** @var WsController $controller */
        list($controller, $actionID) = Module::getInstance()->createController($route);
        $controller->from = $from;
        $controller->userStorage = $this->userStorage;
        $controller->runAction($actionID, $params);
    }
    
    public function onClose(ConnectionInterface $conn)
    {
        $this->userStorage->detach($conn);
        echo "Client disconnected! Total: " . $this->userStorage->count() . "\r\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo $e->getMessage() . "\r\n";
        $conn->close();
    }
}