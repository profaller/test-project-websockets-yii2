<?php

namespace app\server\controllers;


use app\server\components\WsController;
use app\server\models\User;
use yii\helpers\Json;

class ConsoleClientController extends WsController
{
    public function afterAction($action, $result)
    {
        $this->from->send($result);
    }

    public function actionUsers()
    {
        $users = $this->userStorage->getUsers();
        return Json::encode(array_keys($users));
    }

    public function actionTasks($userId)
    {
        $user = $this->userStorage->getUser($userId);
        if ($user) {
            return Json::encode($user->tasks);
        }

        return 'User not found';
    }

    public function actionSendMessage($userId = false, $taskId = false, $message)
    {
        /** @var User[] $users */
        $users = [];
        $userId = (int) $userId;
        if ($userId > 0) {
            $user = $this->userStorage->getUser($userId);
            if ($user) {
                $users[] = $user;
            }
        } else {
            $users = $this->userStorage->getUsers();
            $taskId = false;
        }

        foreach ($users as $user) {
            if ($taskId && !$user->hasTask($taskId)) {
                continue;
            }

            $user->conn->send(Json::encode([
                'action' => 'new-message',
                'message' => $message,
                'taskId' => $taskId
            ]));
        }

        return true;
    }
}