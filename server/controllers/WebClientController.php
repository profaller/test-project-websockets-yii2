<?php

namespace app\server\controllers;


use app\server\components\WsController;
use app\server\models\User;
use yii\helpers\Json;

class WebClientController extends WsController
{
    public function actionGetNewUser()
    {
        $this->from->send(Json::encode([
            'action' => 'user-data',
            'data' => $this->userStorage->generateNewUserData(),
        ]));
    }

    public function actionRegisterClient($id, array $tasks)
    {
        $this->userStorage[$this->from] = new User([
            'id' => $id,
            'tasks' => $tasks,
            'conn' => $this->from,
        ]);
    }
}